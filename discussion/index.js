const express = require('express');
const mongoose = require('mongoose');

const app = express();
const port = 3001;

mongoose.connect(`mongodb+srv://motifaithed:R0n3l0630@zuittbootcampb197-p.m5646t2.mongodb.net/S35-Activity?retryWrites=true&w=majority`,{
    useNewUrlParser: true,  //it allows us to aviod any current and/or future errors while connecting to MongoDB
    useUnifiedTopology: true //set to true to opt in to using the mongodb drivers connection management engine
});

let db = mongoose.connection; //initializes the mongoose connection to the mongodb db by assigning mongoose.connection to the db variable



//listen to the events of the connection by using the on() method of the mongoose connection and logs the details in the console based on the event(error, or successful)
db.on('error', console.error.bind(console,"Connection Error!"));
db.once('open', ()=>{
    console.log('Connected to MongoDB');
});

//CREATING A SCHEMA
const taskSchema = new mongoose.Schema({
    //define the fields with their corresponding data types
    //for a task, it needs a task name and its data type will be string
    name: String,
    status: {
        type: String,
        default: 'Pending'
    }
})

//MODELS
const Task = mongoose.model('Task', taskSchema);

app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.post('/tasks',(req,res)=>{
    Task.findOne({name: req.body.name},(error,result)=>{
        if(error){
            return res.send(error);
        }else if(result != null && result.name == req.body.name){
            return res.send('Duplicate task found');
        }else{
            let newTask = new Task({
                name: req.body.name
            })

            newTask.save((error, savedTask)=>{
                if(error){
                    return console.error(error);
                }else{
                    return res.status(201).send('New Task Created');
                }
            })
        }
    })
})

//get all tasks
app.get('/tasks',(req,res)=>{
    Task.find({},(error, result)=>{
        if(error){
            return res.send(error);
        }else{
            return res.status(200).json({
                tasks: result
            })
        }
    });
})

//activity code here
const userSchema = new mongoose.Schema({
   
    name: String,
    password: String
})
const User = mongoose.model('User', userSchema);

app.post('/signup',(req,res)=>{
    User.findOne({name: req.body.name},(error,result)=>{

        if(error){
            return res.send(error);
        }else if(result != null && result.name == req.body.name){
            return res.send('Duplicate user found');
        }else{
            let newUser = new User({
                name: req.body.name,
                password: req.body.password
            })

            newUser.save((error, savedTask)=>{
                if(error){
                    return console.error(error);
                }else{
                    return res.status(201).send('New User Created');
                }
            })
        }
    })
})

//get all users
app.get('/users',(req,res)=>{
    User.find({},(error, result)=>{
        if(error){
            return res.send(error);
        }else{
            return res.status(200).json({
                tasks: result
            })
        }
    });
})

app.listen(port,()=>{
    console.log(`server is now running on port ${port}`);
})